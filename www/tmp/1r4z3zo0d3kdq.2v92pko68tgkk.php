<div class="form-group">
	<label for="model">Model</label>
		<?php if ($model): ?>
			<div class="form-group has-error">
				<span id="inputError2Status" class="sr-only">(error)</span>
				<span id="helpBlock2" class="help-block has-error"><?= $model ?></span>
			</div>
		<?php endif; ?>
		<input type="text" name="model" id="model" value="<?= $POST['model'] ?>" class="form-control">
</div>
<div class="form-group">
	<label for="color">Farbe</label>
		<?php if ($color): ?>
			<div class="form-group has-error">
				<span id="inputError2Status" class="sr-only">(error)</span>
				<span id="helpBlock2" class="help-block has-error"><?= $color ?></span>
			</div>
		<?php endif; ?>
		<input type="text" name="color" id="color" value="<?= $POST['color'] ?>" class="form-control">
</div>
<div class="form-group">
	<label for="fuel_type">Kraftstoff</label>
		<select name="fuel_type" id="fuel_type" class="form-control">
			<option>Bitte auswählen</option>
			<option value="benzin">Benzin</option>
			<option value="diesel">Diesel</option>
		</select>
</div>
<div class="form-group">
	<label for="year">Baujahr</label>
		<?php if ($year): ?>
			<div class="form-group has-error">
				<span id="inputError2Status" class="sr-only">(error)</span>
				<span id="helpBlock2" class="help-block has-error"><?= $year ?></span>
			</div>
		<?php endif; ?>
		<input type="date" name="year" id="year" class="form-control">
</div>
<div class="form-group">
	<label for="horsepower">Pferdestärken</label>
		<?php if ($horsepower): ?>
			<div class="form-group has-error">
				<span id="inputError2Status" class="sr-only">(error)</span>
				<span id="helpBlock2" class="help-block has-error"><?= $horsepower ?></span>
			</div>
		<?php endif; ?>
		<input type="text" name="horsepower" id="horsepower" class="form-control">
</div>
	<input type="hidden" name="newAdvertisementCar" value="newAdvertisementCar">
<form id="newUser" method="post" action="/register">
	<fieldset>
		<legend>Persönliche Daten</legend>
				<div class="form-group">
					<label for="email">E-Mail Adresse</label></p>
						<?php if ($email): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block has-error"><?= $email ?></span>
						</div>
						<?php endif; ?>
						<?php if ($emailexists): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block has-error"><?= $emailexists ?></span>
						</div>
						<?php endif; ?>
					<input type="email" name="email" id="email" value="<?= $POST['email'] ?>" class="form-control">
				</div>
				<div class="form-group">
					<label for="password">Passwort</label></p>
						<?php if ($password): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $password ?></span>
						</div>
						<?php endif; ?>
						<?php if ($pwvalidate): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $pwvalidate ?></span>
						</div>
						<?php endif; ?>
					<input type="password" name="password" id="password" class="form-control">
				</div>
				<div class="form-group">
					<label for="password2">Passwort wiederholen</label></p>
					<input type="password" name="password2" id="password2" class="form-control">
				</div>
				<div class="form-group">
					<label for="first_name">Vorname</label></p>
						<?php if ($first_name): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $first_name ?></span>
						</div>
						<?php endif; ?>
					<input type="text" name="first_name" id="first_name" value="<?= $POST['first_name'] ?>" 
					class="form-control">
				</div>
				<div class="form-group">
					<label for="last_name">Nachname</label></p>
						<?php if ($last_name): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $last_name ?></span>
						</div>
						<?php endif; ?>
					<input type=text name="last_name" id="last_name" value="<?= $POST['last_name'] ?>" 
					class="form-control">
				</div>
				<div class="form-group">
					<label for="birthday">Geburtstag</label></p>
				<div>(Bitte im folgendem Format eingeben: JJJJ-MM-TT)</div>
						<?php if ($birthday): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $birthday ?></span>
						</div>
						<?php endif; ?>
					<input type="date" name="birthday" id="birthday" value="<?= $POST['birthday'] ?>" 
					class="form-control">
				</div>
				<div class="form-group">
					<label for="phone">Telefon</label></p>
						<?php if ($phone): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $phone ?></span>
						</div>
						<?php endif; ?>
					<input type="text" name="phone" id="phone" value="<?= $POST['phone'] ?>" class="form-control">
				</div>
	</fieldset>
	<fieldset>
		<legend>Adresse</legend>
				<div class="form-group">
					<label for="address">Straße</label></p>
						<?php if ($address): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $address ?></span>
						</div>
						<?php endif; ?>
					<input type="text" name="address" id="address" value="<?= $POST['address'] ?>" class="form-control">
				</div>
				<div class="form-group">
					<label for="address_nr">Nr.</label></p>
						<?php if ($address_nr): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $address_nr ?></span>
						</div>
						<?php endif; ?>
					<input type="text" name="address_nr" id="address_nr" value="<?= $POST['address_nr'] ?>" 
					class="form-control">
				</div>
				<div class="form-group">
					<label for="postal_code">Postleitzahl</label></p>
						<?php if ($postal_code): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $postal_code ?></span>
						</div>
						<?php endif; ?>
					<input type="text" name="postal_code" id="postal_code" value="<?= $POST['postal_code'] ?>" 
					class="form-control">
				</div>
				<div class="form-group">
					<label for="city">Stadt/Dorf/Gemeinde</label></p>
						<?php if ($city): ?>
						<div class="form-group has-error">
							<span id="inputError2Status" class="sr-only">(error)</span>
							<span id="helpBlock2" class="help-block"><?= $city ?></span>
						</div>
						<?php endif; ?>
					<input type="text" name="city" id="city" value="<?= $POST['city'] ?>" class="form-control">
				</div>
			<?php echo $this->render('user/new-user/register-country-state.htm',NULL,get_defined_vars(),0); ?>
	</fieldset>
			<input type="hidden" name="newUser" value="newUser">
			<button type="reset" class="btn btn-danger">Zurücksetzen</button>
			<button type="submit" class="btn btn-primary">Erstellen</button>
			<hr />
			<a href="<?= $BASE . '/' ?>" class = "btn btn-primary">Zurück zum Login</a>
</form>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <base href="<?= $BASE. '/' .$UI ?>" />
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" 
    integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" 
	integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" 
	integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	<!-- JQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- FullCalendar -->
	<link rel='stylesheet' href='../../thirdparty/vendor/fullcalendar/fullcalendar.css' />
	<script src='../../thirdparty/vendor/fullcalendar/lib/jquery.min.js'></script>
	<script src='../../thirdparty/vendor/fullcalendar/lib/moment.min.js'></script>
	<script src='../../thirdparty/vendor/fullcalendar/fullcalendar.js'></script>   
	
    <title><?= $site ?></title>
</head>
<div class="container">
<body> 
    <div class="jumbotron row">
    		<div>
                <h2><?= $page_head ?></h2>
    		</div>
    </div>
    <?php if ($SESSION['user_id'] >0): ?><a href="/logout" class="btn btn-danger float-right">Abmelden</a><?php endif; ?>
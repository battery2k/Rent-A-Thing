<table class="table table-hover table-striped">
	<thead class="thead-inverse">
		<tr>
			<th>Titel</th>
			<th>Beschreibung</th>
			<th>Preis pro Stunde (in €)</th>
			<th>Preis pro Tag (in €)</th>
			<th>Besitzer</th>
			<th>Postleitzahl</th>
			<th>Stadt</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $this->render('advertisment/all/list.htm',NULL,get_defined_vars(),0); ?>
	</tbody>
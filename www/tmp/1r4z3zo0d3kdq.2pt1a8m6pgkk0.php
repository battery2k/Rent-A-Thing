<script>
$(document).ready(function() {

    $("#country").change(function() {
        var val = $(this).val();
        $("#state").html(options[val]);
    });


    var options = [
        "<option value=''></option>",
        "<?php foreach (($user->getStatesByCountryId(1)?:[]) as $value): ?>
        <option value=''><?= $value->state ?></option><?php endforeach; ?>",
        "<?php foreach (($user->getStatesByCountryId(2)?:[]) as $value): ?>
        <option value=''><?= $value->state ?></option><?php endforeach; ?>"
    ];

});
</script>
	<p><label for="country">Land</label></p>
		<select name="country" id="country">
			<option value= "0" selected>Bitte auswählen</option>
			<?php $cnt=0; foreach (($user->getCountries()?:[]) as $country): $cnt++; ?>
			<option value="<?= $cnt ?>"><?= $country->country ?></option>
			<?php endforeach; ?>
		</select>
	<p><label for="state">Bundesland</label></p>
		<select name="state" id="state">
		</select>
<form id ="newAdvertisement" method="post" action ="/advertisement/create">
<h1>Neue Anzeige erstellen</h1>
<?php if (count($errors) >0) echo $this->render('advertisement/new-advertisement/create-error.htm',NULL,get_defined_vars(),0); ?>
<?php echo $this->render('advertisement/new-advertisement/create-category.htm',NULL,get_defined_vars(),0); ?>
<div id="adv-form"></div>
<?php echo $this->render('advertisement/new-advertisement/create-availabilities.htm',NULL,get_defined_vars(),0); ?>
<?php echo $this->render('advertisement/new-advertisement/create-rental-price.htm',NULL,get_defined_vars(),0); ?>
<?php echo $this->render('user/profile/my-addresses.htm',NULL,get_defined_vars(),0); ?>
	<input type="hidden" name="newAdvertisement" value="newAdvertisement">
	<hr />
			<button type="reset" class="btn btn-danger">Zurücksetzen</button>
			<button type="submit" class="btn btn-primary">Erstellen</button>
			<hr />
			<a href="<?= $BASE . '/' ?>" class = "btn btn-primary">Zurück zum Login</a>
</form>
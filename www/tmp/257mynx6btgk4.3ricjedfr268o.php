<?php foreach (($advertisements?:[]) as $entry): ?>
	<tr>
		<td><?= $entry['title'] ?></td>
		<td><?= $entry['description'] ?></td>
		<td><?= str_replace('.',',',trim($entry['rental_price_hour'])) ?> €</td>
		<td><?= str_replace('.',',',trim($entry['rental_price_day'])) ?> €</td>
		<td><a href="<?= $BASE . '/advertisement/'
		.$entry->getUser()->first_name.$entry->getUser()->last_name.'/'.$entry['user_id'].'/all' ?>">
		<?= $entry->getUser()->first_name.' '.$entry->getUser()->last_name ?></a></td>
		<td><?= $entry->getAddress()->postal_code ?></td>
		<td><?= $entry->getAddress()->city ?></td>
		<td><a href="<?= $BASE . '/advertisement/single/fulladv/' . $entry['id'] ?>" 
		class ="btn btn-primary">Anzeigen</a></td>
	</tr>
<?php endforeach; ?>
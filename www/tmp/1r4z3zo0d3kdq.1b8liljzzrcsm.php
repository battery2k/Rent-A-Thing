<h2>Alle Anzeigen</h2>
<div>
<table class="table table-hover table-striped">
	<thead class="thead-inverse">
		<tr>
			<th>Titel</th>
			<th>Beschreibung</th>
			<th>Preis pro Stunde</th>
			<th>Preis pro Tag</th>
			<th>Besitzer</th>
			<th>Postleitzahl</th>
			<th>Stadt</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $this->render('advertisement/all/list.htm',NULL,get_defined_vars(),0); ?>
	</tbody>
</table>
</div>
<div class="form-group">
	<label for="model">Model</label>
			<input type="text" name="model" id="model" class="form-control">
</div>
<div class="form-group">
	<label for="color">Farbe</label>
		<input type="text" name="color" id="color" class="form-control">
</div>
<div class="form-group">
	<label for="fuel_type">Kraftstoff</label>
		<select name="fuel_type" id="fuel_type" class="form-control">
			<option>Bitte auswählen</option>
			<option value="benzin">Benzin</option>
			<option value="diesel">Diesel</option>
		</select>
</div>
<div class="form-group">
	<label for="year">Baujahr</label>
		<input type="date" name="year" id="year" class="form-control">
</div>
<div class="form-group">
	<label for="horsepower">Pferdestärken</label>
		<input type="text" name="horsepower" id="horsepower" class="form-control">
</div>
	<input type="hidden" name="newAdvertisementCar" value="newAdvertisementCar">
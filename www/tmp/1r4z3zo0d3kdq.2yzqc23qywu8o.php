<h3>Meine aktuellen Anzeigen:</h3>
<table class="table table-hover table-bordered table-striped">
	<thead class="thead-inverse">
		<tr>
			<th class="col">Anzeigen Titel</th>
			<th class="col">Erstellt am</th>
			<th class="col">Bearbeiten</th>
			<th class="col">Löschen</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach (($user->getAdvertisements()?:[]) as $adv): ?>
		<tr scope="row">
		 	<td><?= trim($adv->title) ?></td>
			<td><?= trim($adv->getTimeOfCreation()) ?></td>
			<td><a href="" class="btn btn-primary">Bearbeiten</a></td>
			<td><a href="" class="btn btn-danger">Löschen</a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
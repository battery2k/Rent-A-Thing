<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th class="col">Anzeigen Titel</th>
			<th class="col">Erstellt am</th>
			<th class="col">Test</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach (($myadv?:[]) as $myentry): ?>
		<tr>
			<td>test</td>
			<td><?= trim($myentry->getUserAdvertisment()->title) ?></td>
			<td><?= trim($myentry->getUserAdvertisment()->time_of_creation) ?></td>
			<td><a href="" class="btn btn-primary">Bearbeiten</a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
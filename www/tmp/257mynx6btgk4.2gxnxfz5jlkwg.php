<form method="post" action="/authenticate">
  		<div class="form-group">
    		<label for="email">Email</label>
      			<input type="email" name="email" class="form-control" id="email" placeholder="Email">
  		</div>
  		<div class="form-group">
   			<label for="password">Passwort</label>
      			<input type="password" name="password" class="form-control" id="password" placeholder="Passwort">
  		</div>
  		<div class="form-group">
    		<p><a href="/register">Noch nicht registriert?</a></p>
      			<button type="submit" class="btn btn-default">Sign in</button>
  		</div>
</form>

<form id="newUser" method="post" action="user/new User">
	<fieldset>
		<p><label for="email">E-Mail Adresse</label></p>
		<input type="email" name="email" id="email">
		<p><label for="password">Passwort</label></p>
		<input type="password" name="password" id="password">
		<p><label for="password2">Passwort wiederholen</label></p>
		<input type="password" name="password2" id="password2">
		<p><label for="first_name">Vorname</label></p>
		<input type="text" name="first_name" id="first_name">
		<p><label for="last_name">Nachname</label></p>
		<input type=text name="last_name" id="last_name">
		<p><label for="birthday">Geburtstag</label></p>
		<input type="text" name="birthday" id="birthday">
		<p><label for="phone">Telefon</label></p>
		<input type="text" name="phone" id="phone">
	</fieldset>
	<fieldset>
		<p><label for="address">Straße</label></p>
		<input type="text" name="address" id="address">
		<p><label for="address_nr">Nr.</label></p>
		<input type="text" name="address_nr" id="address_nr">
		<p><label for="postal_code">Postleitzahl</label></p>
		<input type="text" name="postal_code" id="postal_code">
		<p><label for="city">Stadt/Dorf/Gemeinde</label></p>
		<input type="text" name="city" id="city">
	<?php echo $this->render('app/views/user/new user/register-state.htm',NULL,get_defined_vars(),0); ?>
	</fieldset>
		<input type="hidden" name="newUser" value="newUser">
		<button type="submit" class="btn btn-primary">Erstellen</button>
</form>
<script>
$(document).ready(function () {
    $("#country").change(function () {
        var val = $(this).val();
        if (val == "0") {
            $("#state_id").html("<option value=''></option>");
        } else if (val == "Deutschland") {
            $("#state_id").html(
            		"<?php foreach (($user->getStatesByCountryId(1)?:[]) as $value): ?>
                 	<option value=<?= $value->state ?>><?= $value->state ?></option><?php endforeach; ?>"
                    		);
        } else if (val == "Österreich") {
            $("#state_id").html(
            		"<?php foreach (($user->getStatesByCountryId(2)?:[]) as $value): ?>
                    <option value=<?= $value->state ?>><?= $value->state ?></option><?php endforeach; ?>"
                    );
        }
    });
});
</script>
	<div class="form-group">
		<label for="country">Land</label></p>
			<select name="country" id="country" class="form-control">
				<option value= "0" selected>Bitte auswählen</option>
				<?php foreach (($user->getCountries()?:[]) as $value): ?>
				<option value="<?= $value->country ?>" name='country'><?= $value->country ?></option>
				<?php endforeach; ?>
			</select>
	</div>	
	<div class="form-group">
		<label for="state_id">Bundesland</label></p>
			<select name="state_id" id="state_id" class="form-control">
			</select>
	</div>
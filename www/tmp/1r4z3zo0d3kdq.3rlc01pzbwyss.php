  	<?php foreach (($advertisments?:[]) as $entry): ?>
		<tr>
			<td><?= $entry['title'] ?></td>
			<td><?= $entry['description'] ?></td>
			<td><?= $entry['rental_price_hour'] ?></td>
			<td><?= $entry['rental_price_day'] ?></td>
			<td><?= $entry->getUser()->first_name.' '.$entry->getUser()->last_name ?></td>
			<td><?= $entry->getAddress()->postal_code ?></td>
			<td><?= $entry->getAddress()->city ?></td>
			<td><a href="<?= $BASE . '/advertisment/single/fulladv/' . $entry['id'] ?>" class = "btn btn-primary">Anzeigen</a></td>
		</tr>
	<?php endforeach; ?>

<script>
$(document).ready(function () {
    $("#category").change(function () {
        var val = $(this).val();
        if (val == "0") {
            $("#name").html("<option></option>");
        } else if (val == "car") {
            $("#name").html(
            		"<?php foreach (($advertisement->getCategoriesByType(car)?:[]) as $value): ?>
                 	<option value=<?= $value->id ?>><?= $value->name ?></option><?php endforeach; ?>"
                    		);
           	$("#adv-form").load('advertisement/new-advertisement/create-car.htm');

        } else if (val == "rehearsal_room") {
            $("#name").html(
            		"<?php foreach (($advertisement->getCategoriesByType(rehearsal_room)?:[]) as $value): ?>
                    <option value=<?= $value->id ?>><?= $value->name ?></option><?php endforeach; ?>"
                    );
            $("#adv-form").load('advertisement/new-advertisement/create-rehearsal-room.htm')
        }
    });
});
</script>
	
	<fieldset>
		<legend>Kategorien</legend>
			<div class="form-group">
				<label for="category">Kategorie</label>
					<select name="category" id="category" class="form-control" required>
						<option value="0">Bitte auswählen</option>
						<option value="car" name="category">Auto</option>
						<option value="rehearsal_room" name="category">Musik</option>
					</select>
			</div>
			<div class="form-group">
				<label for="name">Typ</label></p>
					<select name="name" id="name" class="form-control" required>
					</select>
			</div>
	</fieldset>
<h2>Anzeige</h2>
<table class="table table-hover table-striped">
	<thead class="thead-inverse">
		<tr>
			<th>Titel</th>
			<th>Kategorie</th>
			<th>Beschreibung</th>
			<th>Preis pro Stunde</th>
			<th>Preis pro Tag</th>
			<th>Besitzer</th>
			<th>Postleitzahl</th>
			<th>Stadt</th>
			<th>Verfügbarkeit</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach (($advertisement?:[]) as $entry): ?>
		<tr>
			<td><?= trim($entry['title']) ?></td>
	 	<td>
	 		<?php foreach (($entry->getCategories()?:[]) as $category): ?>
					<div><?= trim($category->name) ?></div>
			<?php endforeach; ?>
		</td> 
			<td><?= trim($entry['description']) ?></td>
			<td><?= str_replace('.',',',trim($entry['rental_price_hour'])) ?> €</td>
			<td><?= str_replace('.',',',trim($entry['rental_price_day'])) ?> €</td>
			<td><?= trim($entry->getUser()->first_name.' '.$entry->getUser()->last_name) ?></td>
			<td><?= trim($entry->getAddress()->postal_code) ?></td>
			<td><?= trim($entry->getAddress()->city) ?></td>
			<td>
				<?php echo $this->render('availability/fulladv-availabilities.htm',NULL,get_defined_vars(),0); ?>
			</td>
		</tr>
	<?php endforeach; ?>	
	</tbody>
</table>

<div id='calendar'></div>
<script>
$(document).ready(function() {
	$('#calendar').fullCalendar({//Start of options
		header: {
			left:	'title today',
			center:	'',
			right:	'listMonth,agendaWeek,agendaDay prev,next'
		},
	    buttonText: {
	        listMonth: 'Monat',
	        agendaWeek: 'Woche',
	        agendaDay: 'Tag',
	        today: 'Heute'
	    },
		firstDay: 1,
		defaultView:'agendaWeek',
		views:{
			month:{
				showNonCurrentDates:false
			}
		},
		editable: true,
	    eventSources: [
	         {
	             events: function(start, end, timezone, callback) {
	                 $.ajax({
	                 url: "<?= $BASE . '/advertisement/single/availabilityJSON/' . $PARAMS['id'] ?>",
	                 dataType: 'json',
	                 data: 
	                 {
	                	start: start.unix(),
	                 	end: end.unix()
	                 },
	                 success: function(msg) {
	                     var events = msg.events;
	                     callback(events);
	                 }
	                 });
	             },
	             rendering: 'background'
	         },
	     ]
	});
});
</script>
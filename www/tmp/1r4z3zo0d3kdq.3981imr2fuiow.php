<h2><?= $userdetails->first_name ?> <?= $userdetails->last_name ?> 's Anzeigen</h2>
<table class="table table-hover table-striped">
	<thead class="thead-inverse">
		<tr>
			<th>Titel</th>
			<th>Beschreibung</th>
			<th>Preis pro Stunde</th>
			<th>Preis pro Tag</th>
			<th>Postleitzahl</th>
			<th>Stadt</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach (($user?:[]) as $entry): ?>
		<tr>
			<td><?= $entry['title'] ?></td>
			<td><?= $entry['description'] ?></td>
			<td><?= str_replace('.',',',trim($entry['rental_price_hour'])) ?> €</td>
			<td><?= str_replace('.',',',trim($entry['rental_price_day'])) ?> €</td>
			<td><?= $entry->getAddress()->postal_code ?></td>
			<td><?= $entry->getAddress()->city ?></td>
			<td><a href="<?= $BASE . '/advertisement/single/fulladv/' . $entry['id'] ?>" 
			class ="btn btn-primary">Anzeigen</a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
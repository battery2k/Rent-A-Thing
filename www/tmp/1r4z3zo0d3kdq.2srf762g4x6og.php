<div class="alert alert-danger" role="alert">
	<strong>Folgende Fehler sind aufgetreten:</strong><br/>
  	<?php if ($title): ?><strong>Titel</strong><p><?= $title ?></p><?php endif; ?>
  	<?php if ($description): ?><strong>Beschreibung</strong><p><?= $description ?></p><?php endif; ?>
  	<?php if ($model): ?><strong>Model</strong><p><?= $model ?></p><?php endif; ?>
  	<?php if ($color): ?><strong>Farbe</strong><p><?= $color ?></p><?php endif; ?>
  	<?php if ($fuel_type): ?><strong>Kraftstoff</strong><p><?= $fuel_type ?></p><?php endif; ?>
  	<?php if ($year): ?><strong>Baujahr</strong><p><?= $year ?></p><?php endif; ?>
  	<?php if ($horsepower): ?><strong>Pferdestärken (PS)</strong><p><?= $horsepower ?></p><?php endif; ?>
  	<?php if ($size): ?><strong>Größe des Raumes</strong><p><?= $size ?></p><?php endif; ?>
  	<?php if ($equipment): ?><strong>Ausstattung</strong><p><?= $equipment ?></p><?php endif; ?>
  	<?php if ($rental_price_hour): ?><strong>Preis pro Stunde</strong><p><?= $rental_price_hour ?></p><?php endif; ?>
  	<?php if ($rental_price_day): ?><strong>Preis pro Tag</strong><p><?= $rental_price_day ?></p><?php endif; ?>
</div>
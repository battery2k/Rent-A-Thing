<?php
namespace Models;

class AvailabilityModel extends DbModel {
	
	public function __construct($db, $table = 'availability'){
		parent::__construct($db, $table);
	}
	
	/**
	 * Returns single Advertisement Details from DB by the Availability 'advertisement_id'
	 *
	 * @return DbModel
	 */
	public function getAdvertisement() {
		return (new AdvertisementModel($this->db))->getById($this->advertisement_id);
	}
	
	/**
	 * Returns an event array that contains the availabilities of an single advertisement for the calendar.
	 *
	 * @param $id
	 * @return array
	 */
	public function getCalendarAvailabilitiesById($id){
		$availabilities = $this->getByAdvertisementId($id);
		$events = array();
		foreach($availabilities as $value){
			$events['id'] = $value->advertisement_id;
			$events['start'] = $value->avail_day_from.'T'.$value->avail_time_from;
			$events['end'] = $value->avail_day_to.'T'.$value->avail_time_to;
			
		};
		$events['title'] = $this->getAdvertisement()->title;
		return array("events"=>array(($events)));
	}	
	public function getCalendarAvailabilitiesByIdAsJSON($id){
		return json_encode($this->getCalendarAvailabilitiesById($id));
	}
	
	/**
	 * Loads all availabilities by AdvertisementId
	 *
	 * @param number $id
	 * @return DbModel[]
	 */
	public function getByAdvertisementId($id){
		$this->load(array('advertisement_id=?',$id));
		return $this->query;
	}
	
}
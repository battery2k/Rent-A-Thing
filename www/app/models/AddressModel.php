<?php
namespace Models;

class AddressModel extends DbModel {
	
	public function __construct($db, $table = 'address'){
		parent::__construct($db, $table);
	}
	
	/**
	 * Retrieves single User Details from DB by the address 'user_id'
	 *
	 * @return DbModel
	 */
	public function getUser() {
		return (new UserModel($this->db))->getById($this->user_id);
	}
	
	/**
	 * Retrieves single State Details from DB by the address 'state_id'
	 *
	 * @return DbModel
	 */
	public function getState(){
		return (new StateModel($this->db))->getById($this->state_id);
	}
	
	/**
	 * Retrieves the state_id by passing a state name
	 * 
	 * @param string $name
	 * @return int
	 */
	public function getStateIdByName($name){
		return (new StateModel($this->db))->getIdByName($name);	
	}
	
	/**
	 * Retrieves the user_id by passing a email
	 * 
	 * @param string $email
	 * @return int
	 */
	public function getUserIdByEmail($email){
		return (new UserModel($this->db))->getIdByEmail($email);
	}
<<<<<<< HEAD
	/**
	 * Loads all Addresses by UserId
	 *
	 * @param number $id
	 * @return DbModel
	 */
	public function getByUserId($id){
		$this->load(array('user_id=?',$id));
		return $this->query;
	}
=======
>>>>>>> e21ca75a0e395702742134436b007693a3a631c3
}
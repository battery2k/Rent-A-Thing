<?php
namespace Models;

class AdvertismentModel extends DbModel {
	
	/*
	 * Establish connection to Database and Advertisment Table
	 */
	public function __construct($db,$table='advertisment'){
		parent::__construct($db, $table); 
	}
	
	/**
	 * Returns single User Details from DB by the advertisment 'user_id'
	 * 
	 * @return DbModel
	 */
	public function getUser() {
		return (new UserModel($this->db))->getById($this->user_id);
	}
	
	/**
	 * Returns single Address Details from DB by the advertisment 'address_id'
	 * 
	 * @return DbModel
	 */
	public function getAddress() {
		return (new AddressModel($this->db))->getById($this->address_id);
	}
	
	/**
	 * Retrieves the fitting Categories of the Advertisments
	 * 
	 * @return $categories[]
	 */
	public function getCategories() {
		$categoryIds = $this->db->exec(<<<SQL
			SELECT category.id FROM category 
			INNER JOIN advertisment_category ON advertisment_category.category_id = category.id
			WHERE advertisment_category.advertisment_id = ?
SQL
		, [$this->id]);
		
		$categories = [];
		foreach (array_column($categoryIds, 'id') as $id) {
			$categories[] = (new CategoryModel($this->db))->getById($id);
		}
		
		return $categories;
	}
	
	
	/**
	 * Returns stored availability by id.
	 * 
	 * @return DbModel
	 */
	public function getAvailability(){
		return (new AvailabilityModel($this->db))->getByAdvertismentId($this->id);
	}
	
	/**
	 * Returns an array for all days of a given calendar month, having true for available days and false for not
	 * available days.
	 * 
	 * @return boolean[]
	 */
	/*public function getAvailabilitiesForMonth($year = 2017, $month = 1){
		$availabilities = [];
		
		
		// for 0 ... 30
			// foreach availablilites
			// = true
			// = false
		
		return $availabilities;
	}
	
	public function getAvailabilitiesForMonthAsJSON($year = 2017, $month = 1) {
		header('Content-Type: application/json');
		echo(json_encode($this->getAvailabilitiesForMonth($year, $month)));
	}*/
	
	public function getSingleAdvertismentAvailabilityForCalendarByIdAsJSON($id){
		$availability = new AvailabilityModel($this->db);
		$availability->getCalendarAvailabilitiesByIdAsJSON($id);
	}
	
	/**
	 * Loads all advertisments by UserId
	 * 
	 * @param number $id
	 * @return DbModel[]
	 */
	public function getByUserId($id){
		$this->load(array('user_id=?',$id));
		return $this->query;
	}
	/**
	 * Returns & formats the time_of_creation and formats it.
	 *
	 * @return date
	 */
	public function getTimeOfCreation() {
		return (new \DateTime($this->time_of_creation))->format('d.m.Y');
	}
	

}


<?php
namespace Models;

class StateModel extends DbModel {
	
	public function __construct($db, $table = 'state'){
		parent::__construct($db, $table);
	}
	
	/**
	 * Loads a single Country
	 *  @return DbModel
	 */
	public function getCountry(){
		return (new CountryModel($this->db))->getById($this->country_id);
	}
	
	/**
	 * Loads all States by country_id
	 *
	 * @param number $id
	 * @return DbModel
	 */
	public function getByCountryId($id){
		$this->load(array('country_id=?',$id));
		return $this->query;
	}
	
	/**
	 * Returns Id by passing a state name
	 * 
	 * @param string $name
	 * @return int
	 */
	public function getIdByName($name){
		$this->load(array('state=?',$name));
		return $this->id;
	}

}
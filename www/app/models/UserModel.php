<?php
namespace Models;


class UserModel extends DbModel {
	
	public function __construct($db, $table='user'){
		parent::__construct($db, $table);
	}
	/**
	 * Retrieves all existing Advertisements of the User.
	 * 
	 * @return DbModel[]
	 */
	public function getAdvertisements(){
		return (new AdvertisementModel($this->db))->getByUserId($this->id);
	}
	
	/**
	 * Loads all existing Advertisements by user_id.
	 *
	 * @param int
	 * @return DbModel[]
	 */
	public function getAdvertisementsByUserId($id){
		return (new AdvertisementModel($this->db))->getByUserId($id);
	}
	/**
	 * Retrieves all existing Addresses of the User.
	 * 
	 * @return DbModel
	 */
	public function getAddresses(){
		return (new AddressModel($this->db))->getById($this->id);
	}
	
	/**
	 * Loads all existing Addresses by user_id.
	 *
	 * @param int
	 * @return DbModel[]
	 */
	public function getAddressesByUserId($id){
		return (new AddressModel($this->db))->getByUserId($id);
	}
	
	/**
	 * Loads all Countries in the Db.
	 * 
	 * @return DbModel
	 */
	public function getCountries(){
		return (new CountryModel($this->db))->all();
	}
	
	/**
	 * Loads all States in the Db
	 * 
	 * @return DbModel
	 */
	public function getStates(){
		return (new StateModel($this->db))->all();
	}
	
	/**
	 * Loads all States of a single Country by their country_id
	 *
	 * @param number $id
	 * @return DbModel[]
	 */
	public function getStatesByCountryId($id){
		return (new StateModel($this->db))->getByCountryId($id);
	}
	
	/**
	 * Loads User Details by Email Address
	 * to create new user
	 * 
	 * @param string $email
	 */
	public function getByEmail($email){
		$this->load(array('email=?',$email));
		return $this->query[0];
	}

	/**
	 * Returns the user id by entering an email
	 * 
	 * @param string $email
	 * @return int
	 */
	public function getIdByEmail($email){
		$this->load(array('email=?',$email));
		return $this->id;
	}

}
<?php
namespace Models;

class ExceptionModel extends DbModel {
	
	public function __construct($db, $table = 'exception'){
		parent::__construct($db, $table);
	}
	public function getAdvertisement(){
		return (new AdvertisementModel($this->db))->getById($this->advertisement_id);
	}
}
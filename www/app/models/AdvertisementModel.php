<?php
namespace Models;

class AdvertisementModel extends DbModel {
	
	/*
	 * Establish connection to Database and Advertisment Table
	 */
	public function __construct($db,$table='advertisement'){
		parent::__construct($db, $table); 
	}
	
	/**
	 * Returns single User Details from DB by the Advertisement 'user_id'
	 * 
	 * @return DbModel
	 */
	public function getUser() {
		return (new UserModel($this->db))->getById($this->user_id);
	}
	
	/**
	 * Returns single Address Details from DB by the Advertisement 'address_id'
	 * 
	 * @return DbModel
	 */
	public function getAddress() {
		return (new AddressModel($this->db))->getById($this->address_id);
	}
	
	/**
	 * Retrieves the fitting Categories of the Advertisements
	 * 
	 * @return $categories[]
	 */
	public function getCategories() {
		$categoryIds = $this->db->exec(<<<SQL
			SELECT category.id FROM category 
			INNER JOIN advertisement_category ON advertisement_category.category_id = category.id
			WHERE advertisement_category.advertisement_id = ?
SQL
		, [$this->id]);
		
		$categories = [];
		foreach (array_column($categoryIds, 'id') as $id) {
			$categories[] = (new CategoryModel($this->db))->getById($id);
		}
		
		return $categories;
	}
	
	/**
	 * Returns all the Category-Type Names by 'type'
	 * 
	 * @param unknown $type
	 * @return DbModel
	 */
	public function getCategoriesByType($type){
		return (new CategoryModel($this->db))->getByType($type);
	}
	
	
	/**
	 * Returns stored availability by id.
	 * 
	 * @return DbModel
	 */
	public function getAvailability(){
		return (new AvailabilityModel($this->db))->getByAdvertisementId($this->id);
	}
	
	/**
	 * Returns an Advertisement Availability as JSON
	 * 
	 * @param $id
	 * @return string JSON
	 */
	public function getSingleAdvertisementAvailabilityForCalendarByIdAsJSON($id){
		$availability = new AvailabilityModel($this->db);
		return ($availability->getCalendarAvailabilitiesByIdAsJSON($id));
	}
	
	/**
	 * Loads all advertisements by UserId
	 * 
	 * @param number $id
	 * @return DbModel
	 */
	public function getByUserId($id){
		$this->load(array('user_id=?',$id));
		return $this->query;
	}
	/**
	 * Returns & formats the time_of_creation and formats it.
	 *
	 * @return date
	 */
	public function getTimeOfCreation() {
		return (new \DateTime($this->time_of_creation))->format('d.m.Y');
	}
	

}


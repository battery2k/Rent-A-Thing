<?php
namespace Models;

abstract class DbModel extends \DB\SQL\Mapper {
	

	public function __construct(\DB\SQL $db,$table){
		parent::__construct($db,$table);
	}
	
	public function all(){
		$this->load();
		return $this->query;
	}
	public function getById($id){
		$this->load(array('id=?',$id));
		return $this->query[0];
	}
	public function add(){
		$this->copyFrom('POST');
		$this->save();
	}
	public function edit($id){
		$this->load(array('id=?',$id));
		$this->copyFrom('POST');
		$this->save();
	}
	public function delete($id){
		$this->load(array('id=?',$id));
		$this->erase();
	}

}
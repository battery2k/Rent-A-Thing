<?php
namespace Models;

class CategoryModel extends DbModel {
	
	public function __construct($db, $table = 'category'){
		parent::__construct($db, $table);
	}
	
	/**
	 * Loads all Names by Type
	 *
	 * @param number $type
	 * @return DbModel
	 */
	public function getByType($type){
		$this->load(array('type=?',$type));
		return $this->query;
	}
}
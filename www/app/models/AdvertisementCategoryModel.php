<?php
namespace Models;

class AdvertisementCategoryModel extends DbModel {
	
	/*
	 * Establish connection to Database and AdvertisementCategory Table
	 */
	public function __construct($db,$table='advertisement_category'){
		parent::__construct($db, $table);
	}
}
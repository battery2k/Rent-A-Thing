<?php 
namespace Controllers;

class MainController extends Controller {
	
	/**
	 * Displays all Advertisement's Page
	 */
 	public function showAllAdvertisements(){
		$advertisements = new \Models\AdvertisementModel($this->db);
		$this->f3->set('advertisements',$advertisements->all());		
		$this->f3->set('page_head','Rent A Thing!');
		$this->f3->set('view','main/table-advertisement-list.htm');
		
	}
	
	/**
	 * Displays all Advertisements of a specific user ans reroutes in case the url should be changed.
	 */
	public function getUserAdvertisements(){
		$user = new \Models\UserModel($this->db);
		$this->f3->set('user',$user->getAdvertisementsByUserId($this->f3->get('PARAMS.id')));
		$this->f3->set('userdetails',$user->getById($this->f3->get('PARAMS.id')));
		$this->f3->set('page_head','Rent A Thing!');
		$this->f3->set('view','main/user-advertisement-list.htm');
		if($this->f3->get('PARAMS.user_name') !== 
		$this->f3->get('userdetails')->first_name.$this->f3->get('userdetails')->last_name)
			{
				$this->f3->reroute('/advertisement/list');
			}
	}
	
	/**
	 * Display's the Login Page
	 */
	public function showLoginPage(){
		$this->f3->set('page_head','Willkommen bei Rent A Thing!');
		$this->f3->set('view','login/login.htm');
	}
	
	/**
	 * General Logout function
	 */
	public function logout(){
		$this->f3->clear('SESSION');
		$this->f3->reroute('/');
	}

}


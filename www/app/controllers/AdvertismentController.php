<?php
namespace Controllers;

class AdvertismentController extends Controller {

	/**
 	* Show's a full single Advertisment
 	* Check's for Advertisment ID in the URL
 	*/
	public function showFullAdvertisment(){
		$id = $this->f3->get('PARAMS.id');
		$advertisment = new \Models\AdvertismentModel($this->db);
		$advertisment->getSingleAdvertismentAvailabilityForCalendarByIdAsJSON($id);
		$this->f3->set('advertisment',$advertisment->find(array('id=?',$id)));
		$this->f3->set('page_head','Anzeige');
		$this->f3->set('view','advertisment/single/fulladv.htm');
	}
	
	/**
 	* 
 	* 
 	* 
 	*/
	public function validateAdvertismentForm(){
		$post = $this->f3->get('POST');
	
			
		$is_filtered = \Validate::filter_input($post, array(
				'title' => 'trim|sanitize_string',
				'description' => 'trim|sanitize_string',
				'rental_price_hour' => 'trim|sanitize_string',
				'rental_price_day' => 'trim|sanitize_string'
		));
		
		if ($is_filtered === true){
			
			$is_valid = \Validate::is_valid($post, array(
					'title' => 'required|alpha_numeric|max_len,40|min_len,3',
					'description' => 'required|alpha_dash',
					'rental_price_hour' => 'required|numeric',
					'rental_price_day' => 'numeric'
			));
			if ($is_valid === true){
				\Validate::xss_clean($post);
			} else {
				print_r($is_valid);
			}
			
		} else {
			print_r($is_filtered);
		}
			
	}

	/**
	* Create a new Advertisment
 	* Check's if User is logged in and if Formular is already created, else show's empty Formular.
 	*/
	public function create(){
		if ($this->f3->exists('SESSION.user_id') && $this->f3->exists('POST.create')){
				
					$advertisment = new \Models\AdvertismentModel($this->db);
			
					$this->validateAdvertismentForm();
					
					$advertisment->add();
		
					$this->f3->reroute('/advertisment/list');
		} else {
					$this->f3->set('page_head','Neue Anzeige erstellen');
					$this->f3->set('view','advertisment/create.htm');
		}
	} 
}
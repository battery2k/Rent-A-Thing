<?php
namespace Controllers;

class UserController extends Controller {
	
	/**
	 * Show's the User Profile and all his advertisements if exist.
	 * 
	 */
	public function showUserProfile(){
		$user = new \Models\UserModel($this->db);
		$this->f3->set('user',$user->getById($this->f3->get('SESSION.user_id')));
		$this->f3->set('page_head','Rent A Thing!');
		$this->f3->set('view','user/profile/profile.htm');
	}
	
	
	/**
	 * Authenticates the User by retrieving POST Datas from Login Form and check's input with Database.
	 * 
	 */
	public function authenticate(){
		$useremail = $this->f3->get('POST.email');
		$userpassword = $this->f3->get('POST.password');
		if($this->f3->exists('POST.email') && $this->f3->exists('POST.password')){
			sleep(3); // login should take a while , method against brute force attacks
			$this->f3->clean($useremail); // removes HTML Tags
			$this->f3->clean($userpassword); // removes HTML Tags
			$user = new \Models\UserModel($this->db);
			$user->getByEmail($useremail);
			
			if($this->f3->exists('POST.email') && $user->dry()){
				$this->f3->reroute('/');
			}
			if(password_verify($userpassword, $user->password)){
				$this->f3->clear('SESSION');
				$this->f3->set('SESSION.user_id',$user->id);
				$this->f3->set('SESSION.user_name',$user->last_name);
				$this->f3->reroute('/user/'.$this->f3->get('SESSION.user_name').'/profile');
			} else {
				$this->f3->reroute('/');
			}
		}
	}

	/**
	 * Display's register Form, validates it and adds a new User + one Address to the Database.
	 * 
	 */
	public function createNewUser(){
		
		$user = new \Models\UserModel($this->db);
		$address = new \Models\AddressModel($this->db);
		
		$this->f3->set('user',$user);
		$this->f3->set('page_head','Neuen Benutzer erstellen');
		$this->f3->set('view','user/new-user/register.htm');
		if($this->f3->exists('POST.newUser')){
			
			$post= $this->f3->get('POST');
			$validator = new \Validate();
			$validator->sanitize($post);
			$validator->validation_rules(array(
				'email' 	=> 	'required|valid_email',
				'password' 	=> 	'required|alpha_dash|max_len,255|min_len,6',
				'password2'	=>	'required|alpha_dash|max_len,255|min_len,6',
				'first_name'=>	'required|alpha|max_len,45|min_len,2',
				'last_name'	=>	'required|alpha|max_len,45|min_len,2',
				'birthday'	=>	'required|date',
				'phone'		=>	'required|max_len,45|min_len,6',
				'address'	=>	'required|alpha_space|max_len,50|min_len,3',
				'address_nr'=>	'required|alpha_dash|max_len,25|min_len,1',
				'postal_code'=>	'required|alpha_numeric|max_len,8|min_len,4',
				'city'		=>	'required|alpha_space|max_len,50|min_len,2',
				'country'	=>	'required',
				'state_id'	=>	'required'
			));
			$validator->filter_rules(array(
				'email'		=>	'trim|sanitize_email',
				'password'	=>	'trim|sanitize_string',
				'password2'	=>	'trim|sanitize_string',
				'first_name'=>	'trim|sanitize_string',
				'last_name'	=>	'trim|sanitize_string',
				'birthday'	=>	'trim|sanitize_string',
				'phone'		=>	'trim|sanitize_string',
				'address'	=>	'trim|sanitize_string',
				'address_nr'=>	'trim|sanitize_string',
				'postal_code'=>	'trim|sanitize_string',
				'city'		=>	'trim|sanitize_string'
			));
			$validator->xss_clean($post);
			$validated_data = $validator->run($post);
			if($validated_data === false){
				foreach($validator->get_errors_array() as $fields=>$error){
					$this->f3->set($fields, $error);
					var_dump($this->f3->get('POST.addressCheck'));
				}
				$this->f3->set('view','user/new-user/register.htm');

			} elseif ($this->f3->get('POST.password') !== $this->f3->get('POST.password2')){
				$this->f3->set('pwvalidate','Passwörter stimmen nicht überein');
				$this->f3->set('view','user/new-user/register.htm');
				
			} elseif ($user->getByEmail($this->f3->get('POST.email')) !== NULL){
				$this->f3->set('emailexists','Diese Email Adresse ist bereits registriert!');
				$this->f3->set('view','user/new-user/register.htm');	
			} else {
				$hash = password_hash($this->f3->get('POST.password'), PASSWORD_DEFAULT);
				$this->f3->set('POST.password',$hash);
				$user->copyfrom('POST');
				$user->save();
				$stateid = $address->getStateIdByName($this->f3->get('POST.state_id'));
				$this->f3->set('POST.state_id',$stateid);
				$userid = $address->getUserIdByEmail($this->f3->get('POST.email'));
				$this->f3->set('POST.user_id',$userid);
				$address->copyfrom('POST');
				$address->save();
				$this->f3->set('page_head','Erfolgreich registriert!');
				$this->f3->set('view','/user/new-user/register-create.htm');
			}
		}
	}
}

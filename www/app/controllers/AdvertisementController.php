<?php
namespace Controllers;

class AdvertisementController extends Controller {

	/**
 	* Show's a full single Advertisement
 	* Check's for Advertisement ID in the URL
 	*/
	public function showFullAdvertisement(){
		$id = $this->f3->get('PARAMS.id');
		$advertisement = new \Models\AdvertisementModel($this->db);
		$this->f3->set('advertisement',$advertisement->find(array('id=?',$id)));
		$this->f3->set('page_head','Rent A Thing!');
		$this->f3->set('view','advertisement/single/fulladv.htm');
	}
	
	/**
	 * Display's the FullCalendar on a single Advertisement with it's availabilities.
	 */
	public function getFullCalendar(){
		$id = $this->f3->get('PARAMS.id');
		$advertisement = new \Models\AdvertisementModel($this->db);
		header('Content-Type: application/json');
		echo ($advertisement->getSingleAdvertisementAvailabilityForCalendarByIdAsJSON($id));
		exit();
	}
	
	/**
	 * Display's Advertisement Form and validates it.
	 */
	public function createNewAdvertisement(){
		
		$user = new \Models\UserModel($this->db);
		$advertisement = new \Models\AdvertisementModel($this->db);
		$advertisementCategory = new \Models\AdvertisementCategoryModel($this->db);
		$availability = new \Models\AvailabilityModel($this->db);
		$car =	new \Models\CarModel($this->db);
		$rehearsalRoom = new \Models\RehearsalRoomModel($this->db);
		
		$this->f3->set('user',$user->getById($this->f3->get('SESSION.user_id')));
		$this->f3->set('useraddress',$user->getAddressesByUserId($this->f3->get('SESSION.user_id')));
		$this->f3->set('advertisement',$advertisement);
		$this->f3->set('page_head','Rent A Thing!');
		$this->f3->set('view','advertisement/new-advertisement/create.htm');
		if($this->f3->exists('POST.newAdvertisement')){
			$post = $this->f3->get('POST');
			$validator = new \Validate();
			$validator->sanitize($post);
			
			if($this->f3->exists('POST.newAdvertisementCar')){
				$validator->validation_rules(array(
					'title'		=>	'required|alpha_space|max_len,255|min_len,2',
					'description'=>	'required|alpha_space|max_len,500|min_len,10',
					'model' 	=> 	'required|alpha_space|max_len,255|min_len,2',
					'color' 	=> 	'required|alpha_space|max_len,255|min_len,1',
					'fuel_type'	=>	'required',
					'year'		=>	'required|date',
					'horsepower'=>	'required|integer|max_len,45|min_len,2',
					'rental_price_hour' => 'required|float',
					'rental_price_day'	=> 'required|float'
				)); 
				$validator->filter_rules(array(
					'model'		=>	'trim|sanitize_string',
					'color'		=>	'trim|sanitize_string',
					'fuel_type'	=>	'trim|sanitize_string',
					'year'		=>	'trim|sanitize_string',
					'horsepower'=>	'trim|sanitize_string',
					'title'		=>	'trim|sanitize_string',
					'description'=>	'trim|sanitize_string',
					'rental_price_hour' => 'trim|sanitize_string',
					'rental_price_hour'	=> 'trim|sanitize_string'
				));	
			}
			if($this->f3->exists('POST.newAdvertisementRehearsalRoom')){
				$validator->validation_rules(array(
					'title'		=>	'required|alpha_space|max_len,255|min_len,2',
					'description'=>	'required|alpha_space|max_len,500|min_len,10',
					'size' 		=> 	'integer',
					'equipment' => 	'required|alpha_space|max_len,300|min_len,1'
				));
				$validator->filter_rules(array(
					'title'		=>	'trim|sanitize_string',
					'description'=>	'trim|sanitize_string',
					'size'		=>	'trim|sanitize_string',
					'equipment'	=>	'trim|sanitize_string'
				));
			}
			
			$validator->xss_clean($post);
			$validated_data = $validator->run($post);
			if($validated_data === false){
				$this->f3->set('errors',true);
				foreach($validator->get_errors_array() as $fields=>$error){
					$this->f3->set($fields, $error);
					echo 'test';
				}

				$this->f3->set('view','advertisement/new-advertisement/create.htm');
				
			} else {
				$userid 	= 	$this->f3->get('SESSION.user_id');
				$addressid 	=	$this->f3->get('POST.addressCheck');
				$this->f3->set('POST.address_id',$addressid);
				$this->f3->set('POST.user_id',$userid);
				$advertisement->copyfrom('POST');
				$advertisement->save();
			
				$avail_year_from  = $this->f3->get('POST.avail_year_from');
				$avail_month_from =	$this->f3->get('POST.avail_month_from');
				$avail_d_from	  =	$this->f3->get('POST.avail_day_from');
				$avail_day_from	  = "$avail_year_from-$avail_month_from-$avail_d_from";
			
				$avail_year_to  = $this->f3->get('POST.avail_year_to');
				$avail_month_to = $this->f3->get('POST.avail_month_to');
				$avail_d_to		= $this->f3->get('POST.avail_day_to');
				$avail_day_to	= "$avail_year_to-$avail_month_to-$avail_d_to";
				
				$this->f3->set('POST.avail_day_from',$avail_day_from);
				$this->f3->set('POST.avail_day_to',$avail_day_to);
				$advertisementid = $advertisement->get('_id');
				$this->f3->set('POST.advertisement_id',$advertisementid);
				$availability->copyfrom('POST');
				$availability->save();
				
				if($this->f3->get('POST.newAdvertisementCar')){
					$car->copyfrom('POST');
					$car->save();
				} elseif ($this->f3->get('POST.newAdvertisementRehearsalRoom')){
					$rehearsalRoom->copyfrom('POST');
					$rehearsalRoom->save();
				}
				
				$categoryid = $this->f3->get('POST.name');
				$this->f3->set('POST.category_id',$categoryid);
				$advertisementCategory->copyfrom('POST');
				$advertisementCategory->save();
				
				$this->f3->reroute('/user/@user_name/profile');
			}
		}
	}
}

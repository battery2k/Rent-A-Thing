<?php 
namespace Controllers;

abstract class Controller {

	protected $f3;
	protected $db;

		function beforeroute(){
			if($this->f3->get('PATH') !== '/' 
			&& $this->f3->get('PATH') !== '/authenticate' 
			&& $this->f3->get('PATH') !== '/register'
			&& $this->f3->get('PATH') !== '/new-user/create')
			{
				if($this->f3->get('SESSION.user_id') === null){
					$this->f3->reroute('/');
					exit;
				}
			}
		}
		
		function afterroute(){
		echo \Template::instance()->render('layout.htm');
		}
		
		function __construct(){
			$f3 = \Base::instance();
			$this->f3=$f3;
			
				
			$db=new \DB\SQL(
					$f3->get(db_dns),
					$f3->get(db_user),
					$f3->get(db_pass),
					array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
					);
			$this->db = $db;
		}

}
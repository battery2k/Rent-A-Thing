$(document).ready(function() {
	$('#calendar').fullCalendar({//Start of options
		header: {
			left:	'title today',
			center:	'',
			right:	'month,agendaWeek,agendaDay prev,next'
		},
		firstDay: 1,
		defaultView:'agendaWeek',
		views:{
			month:{
				showNonCurrentDates:false
			}
		},
		editable: true,
		events:'../../app/controllers/AdvertismentController.php'
	});
});
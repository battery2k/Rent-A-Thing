# Rent-A-Thing

The Rent-A-Thing Project was created by Andreas Friedrich.

It is my first ever MVC Project for simple training purposes.

It uses the **Fat-Free Framework for PHP**, **FullCalendar - JavaScript Event Calendar** and **Bootstrap CSS Library**



The Application is able to :
 - register new users
 - login
 - logout
 - create new advertisements
 - browse existing advertisements
 - show advertisements and their availabilities
 
 
 TODO:
 - include reservation possibility
 - include exeception possibility
 - include assesment possibility
 - include picture upload possibility
 - include more categories
 - ......
 
 
 